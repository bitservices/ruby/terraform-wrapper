###############################################################################

module TerraformWrapper

###############################################################################

  module Shared

###############################################################################

    class Code

###############################################################################

      include TerraformWrapper::Shared::Logging

###############################################################################

      attr_reader :base
      attr_reader :name
      attr_reader :path

###############################################################################

      def initialize(options:)
        logger.fatal("Code base path must be a string!") unless options["base"].kind_of?(String)
        logger.fatal("Code base path must not be blank!") if options["base"].strip.empty?

        @base = options["base"]

        logger.fatal("Code component name must be a string!") unless options["name"].kind_of?(String)
        logger.fatal("Code component name must not be blank!") if options["name"].strip.empty?

        @name = options["name"]

        @path = File.join(@base, @name)
      end

###############################################################################

      def check()
        return exists
      end

###############################################################################

      def exists()
        return File.directory?(@path)
      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
