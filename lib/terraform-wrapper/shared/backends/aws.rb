###############################################################################

module TerraformWrapper
  #############################################################################

  module Shared
    ###########################################################################

    module Backends
      #########################################################################

      class AWS < Common
        #######################################################################

        include TerraformWrapper::Shared::Logging

        #######################################################################

        @@default_class = 'terraform-state'

        #######################################################################

        @@type = 'aws'

        #######################################################################

        attr_reader :bucket, :encrypt, :key, :region

        #######################################################################

        def initialize(options:, variables:)
          construct(options: options, variables: variables)
        end

        #######################################################################

        def hash
          result = {}

          result['bucket']  = @bucket
          result['region']  = @region
          result['key']     = @key
          result['encrypt'] = @encrypt.to_s

          result['kms_key_id'] = @kms  unless @kms.nil?
          result['role_arn']   = @role unless @role.nil?

          result
        end

        #######################################################################

        private

        #######################################################################

        def specific
          kms  = nil
          role = nil

          logger.fatal("AWS backend mandatory option 'bucket' has not been set!") unless @options.key?('bucket')
          logger.fatal("AWS backend mandatory option 'region' has not been set!") unless @options.key?('region')

          bucket = @options['bucket']

          logger.fatal('AWS backend S3 bucket name must be a string!') unless bucket.is_a?(String)
          logger.fatal('AWS backend S3 bucket name must not be blank!') if bucket.strip.empty?

          region = @options['region']

          logger.fatal('AWS backend S3 bucket region must be a string!') unless region.is_a?(String)
          logger.fatal('AWS backend S3 bucket region must not be blank!') if region.strip.empty?

          key = @options.key?('key') ? @options['key'] : File.join('%<service>s', '%<config>s', "%<component>s#{@@ext}")

          logger.fatal('AWS backend S3 bucket key must be a string!') unless key.is_a?(String)
          logger.fatal('AWS backend S3 bucket key must not be blank!') if key.strip.empty?

          encrypt = @options.key?('encrypt') ? @options['encrypt'] : true

          logger.fatal('AWS backend S3 bucket encryption enabled must be a Boolean!') unless [true,
                                                                                              false].include?(encrypt)

          if @options.key?('kms')
            kms = @options['kms']

            unless kms.is_a?(String)
              logger.fatal('AWS backend S3 bucket encryption KMS key ARN must be a string if specified!')
            end
            if kms.strip.empty?
              logger.fatal('AWS backend S3 bucket encryption KMS key ARN must not be blank if specified!')
            end
          end

          if @options.key?('role')
            role = @options['role']

            logger.fatal('AWS backend role to assume ARN must be a string if specified!') unless role.is_a?(String)
            logger.fatal('AWS backend role to assume ARN must not be blank if specified!') if role.strip.empty?
          end

          @variables.core.keys.map { |sym| sym.to_s }.each do |core|
            unless bucket.include?("%<#{core}>s") || key.include?("%<#{core}>s") ||
                   bucket.include?("%{#{core}}") || key.include?("%{#{core}}")
              logger.fatal("AWS backend S3 bucket name or key must include %<#{core}>s.")
            end
          end

          begin
            bucket = bucket % @variables.identifiers
            region = region % @variables.identifiers
            key    = key    % @variables.identifiers
            kms    = kms    % @variables.identifiers unless kms.nil?
            role   = role   % @variables.identifiers unless role.nil?
          rescue StandardError
            logger.fatal('AWS backend options contain identifiers that are not included in the configuration file!')
          end

          logger.fatal("Key: #{key} is too long for backend of type: #{@@type}") if key.length > 1024

          @bucket  = bucket
          @region  = region
          @key     = key
          @encrypt = encrypt
          @kms     = kms
          @role    = role
        end

        #######################################################################
      end

      #########################################################################
    end

    ###########################################################################
  end

  #############################################################################
end

###############################################################################
