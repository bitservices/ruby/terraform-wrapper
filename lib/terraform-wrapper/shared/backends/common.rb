###############################################################################

module TerraformWrapper
  #############################################################################

  module Shared
    ###########################################################################

    module Backends
      #########################################################################

      class Common
        #######################################################################

        include TerraformWrapper::Shared::Logging

        #######################################################################

        @@ext = '.tfstate'
        @@type

        #######################################################################

        @options
        @variables

        #######################################################################

        def initialize(options:, variables:)
          logger.fatal('This class should not be used directly! Please create a backend-specific class instead!')
        end

        #######################################################################

        def hash
          logger.fatal("The backend specific class should override the 'hash' method to return a hash of parameters for Terraform to set!")
        end

        #######################################################################

        def type
          unless @@type.is_a?(String)
            logger.fatal("The backend specific class should set the 'type' class variable to a string!")
          end

          @@type
        end

        #######################################################################

        private

        #######################################################################

        def construct(options:, variables:)
          @options   = options
          @variables = variables

          specific
        end

        #######################################################################

        def specific
          logger.fatal("The backend specific class should override the 'specific' method to include backend specific validation and setup, or simply return 'true' if it is not required.")
        end

        #######################################################################
      end

      #########################################################################
    end

    ###########################################################################
  end

  #############################################################################
end

###############################################################################
