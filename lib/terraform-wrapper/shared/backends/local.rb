###############################################################################

module TerraformWrapper
  #############################################################################

  module Shared
    ###########################################################################

    module Backends
      #########################################################################

      class Local < Common
        #######################################################################

        include TerraformWrapper::Shared::Logging

        #######################################################################

        @@type = 'local'

        #######################################################################

        attr_reader :path

        #######################################################################

        def initialize(options:, variables:)
          construct(options: options, variables: variables)
        end

        #######################################################################

        def hash
          {
            'path' => @path
          }
        end

        #######################################################################

        private

        #######################################################################

        def specific
          path = if @options.key?('path')
                   @options['path']
                 else
                   File.join(Dir.pwd, 'state', 'terraform', '%<config>s',
                             "%<component>s#{@@ext}")
                 end

          logger.fatal('Local backend path must be a string!') unless path.is_a?(String)
          logger.fatal('Local backend path must not be blank!') if path.strip.empty?

          @variables.core.keys.map { |sym| sym.to_s }.each do |core|
            next if (core == 'service') && path.include?(Dir.pwd)

            unless path.include?("%<#{core}>s") || path.include?("%{#{core}}")
              logger.fatal("Local backend path must include %<#{core}>s.")
            end
          end

          begin
            path = path % @variables.identifiers
          rescue StandardError
            logger.fatal('Local backend options contain identifiers that are not included in the configuration file!')
          end

          directory = File.dirname(path)

          logger.fatal("Failed to create state directory: #{directory}") unless ::TerraformWrapper.create_directory(
            directory: directory, purpose: 'state'
          )

          @path = path
        end

        #######################################################################
      end

      #########################################################################
    end

    ###########################################################################
  end

  #############################################################################
end

###############################################################################
