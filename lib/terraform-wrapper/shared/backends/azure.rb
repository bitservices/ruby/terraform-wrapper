###############################################################################

module TerraformWrapper
  #############################################################################

  module Shared
    ###########################################################################

    module Backends
      #########################################################################

      class Azure < Common
        #######################################################################

        include TerraformWrapper::Shared::Logging

        #######################################################################

        @@type = 'azure'

        #######################################################################

        attr_reader :account, :container, :group, :key

        #######################################################################

        def initialize(options:, variables:)
          construct(options: options, variables: variables)
        end

        #######################################################################

        def hash
          {
            'container_name' => @container,
            'key' => @key,
            'resource_group_name' => @group,
            'storage_account_name' => @account
          }
        end

        #######################################################################

        private

        #######################################################################

        def specific
          logger.fatal("Azure backend mandatory option 'group' has not been set!") unless @options.key?('group')

          group = @options['group']

          logger.fatal('Azure backend group must be a string!') unless group.is_a?(String)
          logger.fatal('Azure backend group must not be blank!') if group.strip.empty?

          account = @options.key?('account') ? @options['account'] : group + 'tf'

          logger.fatal('Azure backend storage account must be a string!') unless account.is_a?(String)
          logger.fatal('Azure backend storage account must not be blank!') if account.strip.empty?

          container = @options.key?('container') ? @options['container'] : 'default'

          logger.fatal('Azure backend storage account container must be a string!') unless container.is_a?(String)
          logger.fatal('Azure backend storage account container must not be blank!') if container.strip.empty?

          key = @options.key?('key') ? @options['key'] : File.join('%<service>s', '%<config>s', "%<component>s#{@@ext}")

          logger.fatal('Azure backend storage account key must be a string!') unless key.is_a?(String)
          logger.fatal('Azure backend storage account key must not be blank!') if key.strip.empty?

          @variables.core.keys.map { |sym| sym.to_s }.each do |core|
            unless container.include?("%<#{core}>s") || key.include?("%<#{core}>s") ||
                   container.include?("%{#{core}}") || key.include?("%{#{core}}")
              logger.fatal("Azure backend container or key must include %<#{core}>s.")
            end
          end

          begin
            group     = group     % @variables.identifiers
            account   = account   % @variables.identifiers
            container = container % @variables.identifiers
            key       = key       % @variables.identifiers
          rescue StandardError
            logger.fatal('Azure backend options contain identifiers that are not included in the configuration file!')
          end

          if key.length > 1024
            logger.fatal("Key: #{key} is too long for backend of type: #{@@type}")
          elsif key.length > 256
            logger.warn("Key for backend of type: #{@@type} exceeds 256 characters. This will not work with the Azure Storage Emulator. If key is not being overriden, consider using less identifiers.")
          end

          @group     = group
          @account   = account
          @container = container
          @key       = key
        end

        #######################################################################
      end

      #########################################################################
    end

    ###########################################################################
  end

  #############################################################################
end

###############################################################################
