###############################################################################

module TerraformWrapper
  #############################################################################

  module Shared
    ###########################################################################

    class Runner
      #########################################################################

      include TerraformWrapper::Shared::Logging

      #########################################################################

      @@lockfile_name = '.terraform.lock.hcl'

      #########################################################################

      attr_reader :binary, :code, :config, :downloaded, :initialised

      #########################################################################

      def initialize(binary:, code:)
        logger.fatal("Terraform code location: #{code.path} does not exist!") unless code.check

        @binary = binary
        @code   = code

        @initialised = false
      end

      #########################################################################

      def download
        parameters = []
        parameters.append('-backend=false')

        logger.fatal('Failed to download Terraform modules.') unless run(action: 'init', parameters: parameters)

        @downloaded = true
      end

      #########################################################################

      def init(config:)
        parameters = []
        parameters.append('-reconfigure')

        config.backend.hash.each do |key, value|
          parameters.append("-backend-config=\"#{key}=#{value}\"")
        end

        config.auths.map(&:auth)

        logger.fatal('Failed to initialise Terraform with backend.') unless run(action: 'init', parameters: parameters)

        @config = config
        @initialised = true
      end

      #########################################################################

      def upgrade
        lockfile_path = File.join(@code.path, @@lockfile_name)

        if File.file?(lockfile_path)
          logger.info("Removing lock file: #{lockfile_path}")
          File.delete(lockfile_path)
        end

        logger.fatal('Lock file removal failed!') if File.file?(lockfile_path)

        parameters = []
        parameters.append('-update')
        logger.fatal('Failed to upgrade Terraform modules!') unless run(action: 'get', parameters: parameters)

        parameters = []
        parameters.append('lock')

        @binary.provider.platforms.each do |platform|
          parameters.append("-platform=\"#{platform}\"")
        end

        logger.fatal('Failed to upgrade Terraform providers!') unless run(action: 'providers', parameters: parameters)
      end

      #########################################################################

      def apply(file: nil)
        logger.fatal('Cannot Terraform apply before initialising backend!') unless initialised

        parameters = []
        parameters.append('-auto-approve')

        if !file.nil? and file.is_a?(String) and !file.strip.empty?
          logger.fatal("Plan file: #{file} does not exist!") unless File.file?(file)
          parameters.append("\"#{file}\"")
        else
          parameters.concat(variable_files)
          parameters.concat(variable_strings)
        end

        logger.fatal('Terraform apply failed!') unless run(action: 'apply', parameters: parameters)
      end

      #########################################################################

      def plan(destroy: false, file: nil)
        logger.fatal('Cannot Terraform plan before initialising backend!') unless initialised

        parameters = []
        parameters.concat(variable_files)
        parameters.concat(variable_strings)

        if !file.nil? and file.is_a?(String) and !file.strip.empty?
          logger.fatal("Failed to create plan directory: #{directory}") unless ::TerraformWrapper.create_directory(
            directory: File.dirname(file), purpose: 'plan'
          )
          parameters.append("-out=\"#{file}\"")
        end

        parameters.append('-destroy') if destroy

        logger.fatal('Terraform plan failed!') unless run(action: 'plan', parameters: parameters)
      end

      #########################################################################

      def destroy
        logger.fatal('Cannot Terraform destroy before initialising backend!') unless initialised

        parameters = []
        parameters.concat(variable_files)
        parameters.concat(variable_strings)
        parameters.append('-auto-approve')

        logger.fatal('Terraform destroy failed!') unless run(action: 'destroy', parameters: parameters)
      end

      #########################################################################

      def import(address: nil, id: nil)
        logger.fatal('Cannot Terraform import before initialising backend!') unless initialised

        logger.fatal('Terraform state address for import must be a string!') unless address.is_a?(String)
        logger.fatal('Terraform state address for import must not be blank!') if address.strip.empty?

        logger.fatal('Identification for infrastructure to import must be a string!') unless id.is_a?(String)
        logger.fatal('Identification for infrastructure to import must not be blank!') if id.strip.empty?

        parameters = []
        parameters.concat(variable_files)
        parameters.concat(variable_strings)

        parameters.append("'#{address}'")
        parameters.append("'#{id}'")

        logger.fatal('Terraform import failed!') unless run(action: 'import', parameters: parameters)
      end

      #########################################################################

      def taint(address: nil)
        logger.fatal('Cannot Terraform taint before initialising backend!') unless initialised

        logger.fatal('Terraform state address for taint must be a string!') unless address.is_a?(String)
        logger.fatal('Terraform state address for taint must not be blank!') if address.strip.empty?

        parameters = []
        parameters.append("'#{address}'")

        logger.fatal('Terraform taint failed!') unless run(action: 'taint', parameters: parameters)
      end

      #########################################################################

      def fmt(check: true)
        parameters = []

        parameters.append('-check') if check.to_s.downcase == 'true' && check

        logger.fatal('Terraform fmt failed!') unless run(action: 'fmt', parameters: parameters)
      end

      #########################################################################

      def validate
        logger.fatal('Cannot Terraform validate before downloading modules!') unless downloaded
        logger.fatal('Terraform validation failed!') unless run(action: 'validate')
      end

      #########################################################################

      private

      #########################################################################

      def run(action:, parameters: [])
        result = false

        parameters.reject! { |item| !item.is_a?(String) or item.strip.empty? }

        cmdline = ["\"#{@binary.path}\"", action].concat(parameters).join(' ')

        logger.info("Starting Terraform, action: #{action}")

        puts("\n" + ('#' * 80) + "\n\n")

        Dir.chdir(@code.path)
        result = system(cmdline) || false

        puts("\n" + ('#' * 80) + "\n\n")

        result
      end

      #########################################################################

      def variable_files
        logger.fatal('Cannot generate variable files until Terraform has been initialised!') unless @initialised

        result = []

        @config.variables.files.each do |file|
          result.append("-var-file=\"#{file}\"")
        end

        result
      end

      #########################################################################

      def variable_strings
        logger.fatal('Cannot generate variable strings until Terraform has been initialised!') unless @initialised

        result = []

        @config.variables.values.each do |key, value|
          result.append("-var=\"#{key}=#{value}\"")
        end

        result
      end

      #########################################################################
    end

    ###########################################################################
  end

  #############################################################################
end

###############################################################################
