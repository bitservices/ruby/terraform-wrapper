###############################################################################

require 'yaml'

###############################################################################

module TerraformWrapper

###############################################################################

  module Shared

###############################################################################

    class Config

###############################################################################

      include TerraformWrapper::Shared::Logging

###############################################################################

      @@config_exts = [ "", ".yaml", ".yml" ]

###############################################################################

      attr_reader :auths
      attr_reader :backend
      attr_reader :base
      attr_reader :code
      attr_reader :name
      attr_reader :path
      attr_reader :service
      attr_reader :variables

###############################################################################

      def initialize(code:, options:)
        logger.fatal("Configuration base path must be a string!") unless options["base"].kind_of?(String)
        logger.fatal("Configuration base path must not be blank!") if options["base"].strip.empty?

        @base = options["base"]

        logger.fatal("Configuration name must be a string!") unless options["name"].kind_of?(String)
        logger.fatal("Configuration name must not be blank!") if options["name"].strip.empty?

        @name = options["name"]

        logger.fatal("Configuration service name must be a string!") unless options["service"].kind_of?(String)
        logger.fatal("Configuration service name must not be blank!") if options["service"].strip.empty?

        @service = options["service"]

        logger.fatal("Configuration authenticator for Azure enabled must be a Boolean!") unless [ true, false ].include?(options["auth-azure"])

        auth_azure = options["auth-azure"]

        logger.fatal("Configuration authenticator for Azure options must be a Hash!") unless options["auth-azure-options"].kind_of?(Hash)

        auth_azure_options = options["auth-azure-options"]

        logger.fatal("Configuration backend name must be a string!") unless options["backend"].kind_of?(String)
        logger.fatal("Configuration backend name must not be blank!") if options["backend"].strip.empty?

        backend = options["backend"]

        logger.fatal("Configuration backend options must be a Hash!") unless options["backend-options"].kind_of?(Hash)

        backend_options = options["backend-options"]

        @code = code
        @path = ::TerraformWrapper.find(base: @base, name: @name, exts: @@config_exts, description: "Configuration")

        yaml = YAML.load(File.read(@path))
        logger.fatal("Invalid YAML in configuration file: #{@path}") unless yaml.kind_of?(Hash)

        identifiers = yaml.key?("identifiers") ? yaml["identifiers"] : Hash.new
        @variables  = TerraformWrapper::Shared::Variables.new(config: @name, component: @code.name, service: @service, identifiers: identifiers)

        if yaml.key?("globals") then
          logger.fatal("Key 'globals' is not a hash in configuration file: #{@path}") unless yaml["globals"].kind_of?(Hash)
          globals = yaml["globals"]

          @variables.add_variables(variables: globals["variables"]) if globals.key?("variables")
        end

        if yaml.key?("terraform") then
          logger.fatal("Key 'terraform' is not a hash in configuration file: #{@path}") unless yaml["terraform"].kind_of?(Hash)
          terraform = yaml["terraform"]

          [ "globals", @code.name ].each do |extra|
            if terraform.key?(extra) then
              logger.fatal("Key '#{extra}' under 'terraform' is not a hash in configuration file: #{@path}") unless terraform[extra].kind_of?(Hash)
              section = terraform[extra]

              @variables.add_variables(variables: section["variables"]) if section.key?("variables")
              @variables.add_files(base: @base, files: section["files"]) if section.key?("files")
            end
          end
        end

        @auths = Array.new
        @auths.append(TerraformWrapper::Shared::Auths::Azure.new(options: auth_azure_options, variables: @variables)) if auth_azure

        if backend == "local" then
          @backend = TerraformWrapper::Shared::Backends::Local.new(options: backend_options, variables: @variables)
        elsif backend == "aws" then
          @backend = TerraformWrapper::Shared::Backends::AWS.new(options: backend_options, variables: @variables)
        elsif backend == "azure" then
          @backend = TerraformWrapper::Shared::Backends::Azure.new(options: backend_options, variables: @variables)
        else
          logger.fatal("Backend: #{backend} is not valid!")
        end
      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
