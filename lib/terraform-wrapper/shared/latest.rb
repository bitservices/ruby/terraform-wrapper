###############################################################################

require 'json'
require 'net/http'
require 'singleton'
require 'uri'

###############################################################################

module TerraformWrapper

###############################################################################

  module Shared

###############################################################################

    class Latest

###############################################################################

      include Singleton

###############################################################################

      include TerraformWrapper::Shared::Logging

###############################################################################

      @version

###############################################################################

      def version
        @version ||= refresh

        return @version
      end

###############################################################################

      private

###############################################################################

      def refresh
        logger.info("Finding latest available Terraform release...")

        response = Net::HTTP.get_response(URI("https://checkpoint-api.hashicorp.com/v1/check/terraform"))

        logger.fatal("Hashicorp Checkpoint did not return status 200 for latest version check!") if response.code != "200"
        logger.fatal("Response body from Hashicorp Checkpoint is not permitted!")                if not response.class.body_permitted?
        logger.fatal("Response body from Hashicorp Checkpoint is empty!")                        if response.body.nil?

        begin
          body = JSON.parse(response.body)
        rescue JSON::ParserError
          logger.fatal("Response body from Hashicorp Checkpoint is not valid JSON!")
        end

        logger.fatal("Hashicorp Checkpoint JSON response did not include latest available Terraform version!") if not body.key?("current_version")
        logger.fatal("Hashicorp Checkpoint indicated latest available version of Terraform is blank!")         if body["current_version"].empty?

        version = body["current_version"]

        logger.success("Latest available Terraform release found: #{version}")

        return version
      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
