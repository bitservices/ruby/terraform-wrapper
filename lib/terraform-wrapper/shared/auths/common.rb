###############################################################################

module TerraformWrapper

###############################################################################

  module Shared

###############################################################################

    module Auths

###############################################################################

      class Common

###############################################################################

        include TerraformWrapper::Shared::Logging

###############################################################################

        @@type

###############################################################################

        @options
        @variables

###############################################################################

        def initialize(options:, variables:)
          logger.fatal("This class should not be used directly! Please create an authenticator-specific class instead!")
        end

###############################################################################

        def auth()
          logger.fatal("The authenticator specific class should override the 'auth' method to complete the authentication process!")
        end

###############################################################################

        def clear()
          logger.fatal("The authenticator specific class should override the 'auth' method to clear any authentication details!")
        end

###############################################################################

        def type()
          logger.fatal("The authenticator specific class should set the 'type' class variable to a string!") unless @@type.kind_of?(String)

          return @@type
        end

###############################################################################

        private

###############################################################################

        def construct(options:, variables:)
          @options   = options
          @variables = variables

          specific
        end

###############################################################################

        def specific()
          logger.fatal("The authenticator specific class should override the 'specific' method to include authenticator specific validation and setup, or simply return 'true' if it is not required.")
        end

###############################################################################

      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
