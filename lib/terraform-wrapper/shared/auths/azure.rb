###############################################################################

module TerraformWrapper

###############################################################################

  module Shared

###############################################################################

    module Auths

###############################################################################

      class Azure < Common

###############################################################################

        include TerraformWrapper::Shared::Logging

###############################################################################

        @@az   = "az"
        @@type = "azure"

###############################################################################

        @keyvault        = nil
        @secret_username = nil
        @secret_password = nil

###############################################################################

        @subscription

###############################################################################

        def initialize(options:, variables:)
          construct(options: options, variables: variables)
        end

###############################################################################

        def auth()
          details = subscription_details(subscription: @subscription)

          subscription = details["id"]
          tenant       = details["tenant"]
          username     = @keyvault.nil? ? nil : secret(vault: @keyvault, name: @secret_username)
          password     = @keyvault.nil? ? nil : secret(vault: @keyvault, name: @secret_password)

          ENV["ARM_SUBSCRIPTION_ID"] = subscription
          ENV["ARM_TENANT_ID"]       = tenant
          ENV["ARM_CLIENT_ID"]       = username unless username.nil?
          ENV["ARM_CLIENT_SECRET"]   = password unless password.nil?
          logger.success("Azure authenticator environment variables set!")
        end

###############################################################################

        def clear()
          ENV.delete("ARM_SUBSCRIPTION_ID")
          ENV.delete("ARM_TENANT_ID")
          logger.info("Azure authenticator environment variables cleared!")
        end

###############################################################################

        private

###############################################################################

        def cli()
          output  = logger.colour ? "yamlc" : "yaml"
          cmdline = "\"#{@@az}\" version --output \"#{output}\""
          return(system(cmdline) || false)
        end

###############################################################################

        def secret(vault:, name:)
          logger.info("Getting secret: #{name}, from key vault: #{vault}...")

          cmdline = "\"#{@@az}\" keyvault secret show --vault-name \"#{vault}\" --name \"#{name}\" --query \"value\" --output \"tsv\""
          stdout  = `#{cmdline}`
          code    = $?.exitstatus

          logger.fatal("Failed to get secret: #{name} from key vault: #{vault}!") if (code != 0 or stdout.strip.empty?)

          return(stdout.strip)
        end

###############################################################################

        def subscription_details(subscription:)
          logger.info("Looking up details for subscription: #{subscription}...")

          cmdline = "\"#{@@az}\" account show --subscription \"#{subscription}\" --query \"{id:id,tenant:tenantId}\" --output \"yaml\""
          stdout  = `#{cmdline}`
          code    = $?.exitstatus

          logger.fatal("Failed to get details for subscription: #{subscription}!") if code != 0

          details = YAML.load(stdout.strip)

          logger.fatal("Returned details did not include the subscription ID!") unless details.key?("id")
          logger.fatal("Returned subscription ID is not a string!") unless details["id"].kind_of?(String)
          logger.fatal("Returned subscription ID is empty!") if details["id"].strip.empty?

          logger.fatal("Returned details did not include the tenant ID!") unless details.key?("tenant")
          logger.fatal("Returned tenant ID is not a string!") unless details["tenant"].kind_of?(String)
          logger.fatal("Returned tenant ID is empty!") if details["tenant"].strip.empty?

          details.transform_values! { |value| value.strip }

          logger.debug("Returned subscription ID: #{details["id"]}")
          logger.debug("Returned tenant ID: #{details["tenant"]}")

          return(details)
        end

###############################################################################

        def specific()
          keyvault = nil

          logger.fatal("Azure CLI must be installed and accessible to use the Azure authenticator.") unless cli

          logger.fatal("Azure authenticator mandatory option 'subscription' has not been set!") unless @options.key?("subscription")
          logger.fatal("Azure authenticator subscription must be a string!") unless @options["subscription"].kind_of?(String)
          logger.fatal("Azure authenticator subscription must not be blank!") if @options["subscription"].strip.empty?

          subscription = @options["subscription"]

          if @options.key?("keyvault") then
            logger.fatal("Azure authenticator keyvault name must be a string if specified!") unless @options["keyvault"].kind_of?(String)
            logger.fatal("Azure authenticator keyvault name must not be blank if specified!") if @options["keyvault"].strip.empty?

            keyvault = @options["keyvault"]

            if @options.key?("username-secret") then
              logger.fatal("Azure authenticator keyvault secret for username must be a string if keyvault name is specified!") unless @options["username-secret"].kind_of?(String)
              logger.fatal("Azure authenticator keyvault secret for username must not be blank if keyvault name is specified!") if @options["username-secret"].strip.empty?

              username = @options["username-secret"]
            else
              username = "terraform-username"
            end

            if @options.key?("password-secret") then
              logger.fatal("Azure authenticator keyvault secret for password must be a string if keyvault name is specified!") unless @options["password-secret"].kind_of?(String)
              logger.fatal("Azure authenticator keyvault secret for password must not be blank if keyvault name is specified!") if @options["password-secret"].strip.empty?

              password = @options["password-secret"]
            else
              password = "terraform-password"
            end
          end

          begin
            @subscription    = subscription % @variables.identifiers
            @keyvault        = keyvault     % @variables.identifiers unless keyvault.nil?
            @secret_username = username     % @variables.identifiers unless keyvault.nil?
            @secret_password = password     % @variables.identifiers unless keyvault.nil?
          rescue
            logger.fatal("Azure authenticator options contain identifiers that are not included in the configuration file!")
          end
        end

###############################################################################

      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
