###############################################################################

module TerraformWrapper

###############################################################################

  module Shared

###############################################################################

    class Variables

###############################################################################

      include TerraformWrapper::Shared::Logging

###############################################################################

      @@variable_files_name =   "tfvars"
      @@variable_files_exts = [ ".tfvars" ]

###############################################################################

      attr_reader :core
      attr_reader :files
      attr_reader :identifiers
      attr_reader :values

###############################################################################

      def initialize(component:, config:, service:, identifiers: Hash.new, sort: false)
        logger.fatal("Identifiers provided must be a hash!") unless identifiers.kind_of?(Hash)

        @core             = Hash.new()
        @core[:component] = component
        @core[:config]    = config
        @core[:service]   = service

        user   = cleanse(variables: identifiers, reserved: @core.keys, downcase: true)
        merged = @core.merge(user)

        @identifiers = sort ? merged.sort.to_h : merged
        @values      = Hash.new
        @files       = Array.new
      end

###############################################################################

      def add_files(base:, files:)
        logger.fatal("Variable files provided must be an array!") unless files.kind_of?(Array)

        files.each do |file|
          logger.fatal("All provided variable file names must be strings!") unless file.kind_of?(String)
          logger.fatal("All provided variable file names must not be blank!") if file.strip.empty?

          path = ::TerraformWrapper.find(base: File.join(base, @@variable_files_name), name: file.strip, exts: @@variable_files_exts, description: "Terraform values file")

          if @files.include?(path) then
            logger.warn("Terraform variables file is included more than once: #{file.strip}")
          else
            @files.append(path)
          end
        end
      end

###############################################################################

      def add_variables(variables:, sort: false)
        logger.fatal("Variables provided must be a hash!") unless variables.kind_of?(Hash)

        cleansed = cleanse(variables: variables, reserved: @values.keys)

        begin
          cleansed = cleansed.map{ |key, value| [ key, value % @identifiers ] }.to_h
        rescue
          logger.fatal("Variables contain identifiers that are not included in the configuration file!")
        end

        merged  = @values.merge(cleansed)
        @values = sort ? merged.sort.to_h : merged
      end

###############################################################################

      def clear_files()
        @files = Array.new
      end

###############################################################################

      def clear_variables()
        @values = Hash.new
      end

###############################################################################

      private

###############################################################################

      def cleanse(variables:, reserved:, downcase: false)
        result = Hash.new

        variables.keys.each do |key|
          logger.fatal("Could not clean variables hash. All keys MUST be strings!") unless key.kind_of?(String)

          sym = downcase ? key.downcase.to_sym : key.to_sym

          logger.fatal("Could not clean variables hash, key: #{sym.to_s} is reserved or already in use and cannot be used!") if reserved.include?(sym)
          logger.fatal("Could not clean variables hash, duplicate key found: #{sym.to_s}!") if result.key?(sym)
          logger.fatal("Could not clean variables hash, value for: #{sym.to_s} is not a string!") unless variables[key].kind_of?(String)
          logger.fatal("Could not clean variables hash, value for: #{sym.to_s} is empty!") if variables[key].strip.empty?

          result[sym] = variables[key].strip
        end

        return result
      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
