###############################################################################

module TerraformWrapper

###############################################################################

  def self.create_directory(directory:, exists: true, purpose: nil)
    return exists if File.directory?(directory)

    result = false

    if not purpose.nil? and purpose.kind_of?(String) and not purpose.strip.empty? then
      @logger.info("Creating directory for: #{purpose}, path: #{directory}")
    else
      @logger.info("Creating directory: #{directory}")
    end

    FileUtils.mkdir_p(directory)

    result = File.directory?(directory)
  end

###############################################################################

  def self.find(base:, name:, exts:, description: "File")
    exts.each do |ext|
      path = File.join(base, name + ext)
      return path if File.file?(path)
    end

    @logger.fatal("#{description} name: #{name} not found in location: #{base}!")
  end

###############################################################################

end

###############################################################################
