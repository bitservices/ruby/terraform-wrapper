###############################################################################

module TerraformWrapper
  #############################################################################

  module Tasks
    ###########################################################################

    class Fmt < ::Rake::TaskLib
      #########################################################################

      include TerraformWrapper::Shared::Logging

      #########################################################################

      @binary
      @code

      #########################################################################

      def initialize(binary:, code:)
        @binary = binary
        @code   = code

        yield self if block_given?

        fmt_task
      end

      #########################################################################

      def fmt_task
        desc 'Manages the formatting of the Terraform code for an infrastructure component.'
        task :fmt, [:check] => :binary do |_t, args|
          args.with_defaults(check: true)

          runner = TerraformWrapper::Shared::Runner.new(binary: @binary, code: @code)

          logger.info("Fmt Terraform component: #{@code.name}...")

          runner.fmt(check: args[:check])
        end
      end

      #########################################################################
    end

    ###########################################################################
  end

  #############################################################################
end

###############################################################################
