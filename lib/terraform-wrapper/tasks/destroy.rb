###############################################################################

module TerraformWrapper
  #############################################################################

  module Tasks
    ###########################################################################

    class Destroy < ::Rake::TaskLib
      #########################################################################

      include TerraformWrapper::Shared::Logging

      #########################################################################

      @binary
      @code
      @options

      #########################################################################

      def initialize(binary:, code:, options:)
        @binary  = binary
        @code    = code
        @options = options

        yield self if block_given?

        destroy_task
      end

      #########################################################################

      def destroy_task
        desc 'Destroys infrastructure with Terraform for a given configuration on an infrastructure component.'
        task :destroy, [:config] => :binary do |_t, args|
          options = @options.merge({ 'name' => args[:config] })

          logger.info('Processing configuration for Terraform destroy...')

          config = TerraformWrapper::Shared::Config.new(code: @code, options: options)
          runner = TerraformWrapper::Shared::Runner.new(binary: @binary, code: @code)

          logger.info("Running Terraform destroy for service: #{config.service}, component: #{@code.name}...")

          runner.init(config: config)
          runner.destroy
        end
      end

      #########################################################################
    end

    ###########################################################################
  end

  #############################################################################
end

###############################################################################
