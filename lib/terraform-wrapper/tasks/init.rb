###############################################################################

module TerraformWrapper
  #############################################################################

  module Tasks
    ###########################################################################

    class Init < ::Rake::TaskLib
      #########################################################################

      include TerraformWrapper::Shared::Logging

      #########################################################################

      @binary
      @code
      @options

      #########################################################################

      def initialize(binary:, code:, options:)
        @binary  = binary
        @code    = code
        @options = options

        yield self if block_given?

        init_task
      end

      #########################################################################

      def init_task
        desc 'Initialises the Terraform infrastructure component and state backend.'
        task :init, [:config] => :binary do |_t, args|
          options = @options.merge({ 'name' => args[:config] })

          logger.info('Processing configuration for Terraform init...')

          config = TerraformWrapper::Shared::Config.new(code: @code, options: options)
          runner = TerraformWrapper::Shared::Runner.new(binary: @binary, code: @code)

          logger.info("Initialising Terraform for service: #{config.service}, component: #{@code.name}...")

          runner.init(config: config)
        end
      end

      #########################################################################
    end

    ###########################################################################
  end

  #############################################################################
end

###############################################################################
