###############################################################################

module TerraformWrapper
  #############################################################################

  module Tasks
    ###########################################################################

    class Clean < ::Rake::TaskLib
      #########################################################################

      include TerraformWrapper::Shared::Logging

      #########################################################################

      @@glob_directories = {
        'Terraform Modules' => '**/.terraform'
      }

      #########################################################################

      @@glob_files = {
        'Crash Logs' => '**/crash.log',
        'Graph Images' => '**/graph.png',
        'Terraform States' => '**/*.tfstate',
        'Terraform State Backups' => '**/*.tfstate.backup'
      }

      #########################################################################

      @code

      #########################################################################

      def initialize(code:)
        @code = code

        yield self if block_given?

        clean_task
      end

      #########################################################################

      def clean_task
        desc 'Cleans a Terraform infrastructure component workspace of any downloaded providers and modules.'
        task :clean do |_t, _args|
          logger.info("Cleaning Terraform component: #{@code.name}...")

          directory_counter = 0
          @@glob_directories.each do |key, value|
            logger.info("Cleaning directories: #{key}")
            directory_item_counter = 0

            directories = Dir.glob(File.join(@code.path, value))
            directories.each do |directory|
              if delete_directory(directory: directory)
                directory_item_counter += 1
                directory_counter      += 1
              end
            end
            logger.success("Cleaned: #{directory_item_counter} directories.")
          end

          file_counter = 0
          @@glob_files.each do |key, value|
            logger.info("Cleaning files: #{key}")
            file_item_counter = 0

            files = Dir.glob(File.join(@code.path, value))
            files.each do |file|
              if delete_file(file: file)
                file_item_counter += 1
                file_counter      += 1
              end
            end
            logger.success("Cleaned: #{file_item_counter} files.")
          end

          logger.success("Cleaned total: #{directory_counter} directories and: #{file_counter} files.")
        end
      end

      #########################################################################

      private

      #########################################################################

      def delete_directory(directory:)
        result = false

        if File.directory?(directory)
          begin
            FileUtils.remove_dir(directory)
            result = true
            logger.info("Deleted directory: #{directory}")
          rescue StandardError
            result = false
            logger.warn("Failed to delete directory: #{directory}")
          end
        end

        result
      end

      #########################################################################

      def delete_file(file:)
        result = false

        if File.file?(file)
          begin
            File.delete(file)
            result = true
            logger.info("Deleted file: #{file}")
          rescue StandardError
            result = false
            logger.warn("Failed to delete file: #{file}")
          end
        end

        result
      end

      #########################################################################
    end

    ###########################################################################
  end

  #############################################################################
end

###############################################################################
