###############################################################################

module TerraformWrapper
  #############################################################################

  module Tasks
    ###########################################################################

    class Upgrade < ::Rake::TaskLib
      #########################################################################

      include TerraformWrapper::Shared::Logging

      #########################################################################

      @binary
      @code

      #########################################################################

      def initialize(binary:, code:)
        @binary = binary
        @code   = code

        yield self if block_given?

        upgrade_task
      end

      #########################################################################

      def upgrade_task
        desc 'Upgrades the Terraform infrastructure component modules, providers and lock file.'
        task upgrade: [:binary] do |_t, _args|
          runner = TerraformWrapper::Shared::Runner.new(binary: @binary, code: @code)

          logger.info("Upgrading Terraform component: #{@code.name}...")

          runner.upgrade
        end
      end

      #########################################################################
    end

    ###########################################################################
  end

  #############################################################################
end

###############################################################################
