###############################################################################

module TerraformWrapper
  #############################################################################

  module Tasks
    ###########################################################################

    class Validate < ::Rake::TaskLib
      #########################################################################

      include TerraformWrapper::Shared::Logging

      #########################################################################

      @binary
      @code

      #########################################################################

      def initialize(binary:, code:)
        @binary = binary
        @code   = code

        yield self if block_given?

        validate_task
      end

      #########################################################################

      def validate_task
        desc 'Validates the Terraform code for an infrastructure component. Will also check formatting by default.'
        task :validate, [:fmt] => :binary do |_t, args|
          args.with_defaults(fmt: true)

          runner = TerraformWrapper::Shared::Runner.new(binary: @binary, code: @code)

          logger.info("Validating Terraform component: #{@code.name}...")

          runner.download
          runner.validate
          runner.fmt if args[:fmt].to_s.downcase == 'true'
        end
      end

      #########################################################################
    end

    ###########################################################################
  end

  #############################################################################
end

###############################################################################
