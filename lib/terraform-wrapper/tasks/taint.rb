###############################################################################

module TerraformWrapper
  #############################################################################

  module Tasks
    ###########################################################################

    class Taint < ::Rake::TaskLib
      #########################################################################

      include TerraformWrapper::Shared::Logging

      #########################################################################

      @binary
      @code
      @options

      #########################################################################

      def initialize(binary:, code:, options:)
        @binary  = binary
        @code    = code
        @options = options

        yield self if block_given?

        taint_task
      end

      #########################################################################

      def taint_task
        desc 'Taint a piece of existing infrastructure so that Terraform recreates it at next apply.'
        task :taint, %i[config address] => :binary do |_t, args|
          options = @options.merge({ 'name' => args[:config] })

          logger.info('Processing configuration for Terraform taint...')

          config = TerraformWrapper::Shared::Config.new(code: @code, options: options)
          runner = TerraformWrapper::Shared::Runner.new(binary: @binary, code: @code)

          logger.info("Running Terraform taint for service: #{config.service}, component: #{@code.name}...")

          runner.init(config: config)
          runner.taint(address: args[:address])
        end
      end

      #########################################################################
    end

    ###########################################################################
  end

  #############################################################################
end

###############################################################################
