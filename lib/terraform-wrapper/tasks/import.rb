###############################################################################

module TerraformWrapper
  #############################################################################

  module Tasks
    ###########################################################################

    class Import < ::Rake::TaskLib
      #########################################################################

      include TerraformWrapper::Shared::Logging

      #########################################################################

      @binary
      @code
      @options

      #########################################################################

      def initialize(binary:, code:, options:)
        @binary  = binary
        @code    = code
        @options = options

        yield self if block_given?

        import_task
      end

      #########################################################################

      def import_task
        desc 'Import a piece of existing infrastructure into Terraform state for a given configuration on an infrastructure component.'
        task :import, %i[config address id] => :binary do |_t, args|
          options = @options.merge({ 'name' => args[:config] })

          logger.info('Processing configuration for Terraform import...')

          config = TerraformWrapper::Shared::Config.new(code: @code, options: options)
          runner = TerraformWrapper::Shared::Runner.new(binary: @binary, code: @code)

          logger.info("Running Terraform import for service: #{config.service}, component: #{@code.name}...")

          runner.init(config: config)
          runner.import(address: args[:address], id: args[:id])
        end
      end

      #########################################################################
    end

    ###########################################################################
  end

  #############################################################################
end

###############################################################################
