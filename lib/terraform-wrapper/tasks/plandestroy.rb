###############################################################################

module TerraformWrapper
  #############################################################################

  module Tasks
    ###########################################################################

    class PlanDestroy < ::Rake::TaskLib
      #########################################################################

      include TerraformWrapper::Shared::Logging

      #########################################################################

      @binary
      @code
      @options

      #########################################################################

      def initialize(binary:, code:, options:)
        @binary  = binary
        @code    = code
        @options = options

        yield self if block_given?

        plan_destroy_task
      end

      #########################################################################

      def plan_destroy_task
        desc 'Creates a Terraform destroy plan for a given configuration on an infrastructure component.'
        task :"plan-destroy", %i[config out] => :binary do |_t, args|
          options = @options.merge({ 'name' => args[:config] })

          logger.info('Processing configuration for Terraform destroy plan...')

          config = TerraformWrapper::Shared::Config.new(code: @code, options: options)
          runner = TerraformWrapper::Shared::Runner.new(binary: @binary, code: @code)

          logger.info("Running Terraform destroy plan for service: #{config.service}, component: #{@code.name}...")

          runner.init(config: config)
          runner.plan(destroy: true, file: args[:out])
        end
      end

      #########################################################################
    end

    ###########################################################################
  end

  #############################################################################
end

###############################################################################
