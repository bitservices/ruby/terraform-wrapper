###############################################################################

require 'rake/tasklib'

###############################################################################

require_relative 'tasks/apply'
require_relative 'tasks/binary'
require_relative 'tasks/clean'
require_relative 'tasks/destroy'
require_relative 'tasks/fmt'
require_relative 'tasks/import'
require_relative 'tasks/init'
require_relative 'tasks/plan'
require_relative 'tasks/plandestroy'
require_relative 'tasks/taint'
require_relative 'tasks/upgrade'
require_relative 'tasks/validate'

###############################################################################
