###############################################################################

require_relative 'terraform-wrapper/shared'
require_relative 'terraform-wrapper/tasks'
require_relative 'terraform-wrapper/version'

###############################################################################

require_relative 'terraform-wrapper/common'

###############################################################################

module TerraformWrapper
  #############################################################################

  @logger = TerraformWrapper::Shared::Logging.logger_for('TerraformWrapper')

  #############################################################################

  @logger.info("Terraform Wrapper for Ruby - version: #{TerraformWrapper::VERSION}")

  #############################################################################

  def self.deployment_tasks(component:, service:, options: {})
    @logger.info("Building tasks for service: #{service}, component: #{component}...")

    @logger.fatal('Options must be specified as a hash!') unless options.is_a?(Hash)

    binary_options = {}
    binary_options['base'] =
      options.key?('binary-base') ? options['binary-base'] : File.join(Dir.pwd, 'vendor', 'terraform')
    binary_options['version'] =
      options.key?('binary-version') ? options['binary-version'] : Shared::Latest.instance.version

    code_options = {}
    code_options['base'] = options.key?('code-base') ? options['code-base'] : File.join(Dir.pwd, 'terraform')
    code_options['name'] = component

    config_options = {}
    config_options['auth-azure'] =
      options.key?('config-auth-azure') ? options['config-auth-azure'] : false
    config_options['auth-azure-options'] =
      options.key?('config-auth-azure-options') ? options['config-auth-azure-options'] : {}
    config_options['base'] =
      options.key?('config-base') ? options['config-base'] : File.join(Dir.pwd, 'config')
    config_options['backend'] =
      options.key?('config-backend') ? options['config-backend'] : 'local'
    config_options['backend-options'] =
      options.key?('config-backend-options') ? options['config-backend-options'] : {}
    config_options['service'] = service

    provider_options = {}
    provider_options['platforms'] = options.key?('provider-platforms') ? options['provider-platforms'] : []

    provider = TerraformWrapper::Shared::Provider.new(options: provider_options)
    binary   = TerraformWrapper::Shared::Binary.new(options: binary_options, provider: provider)
    code     = TerraformWrapper::Shared::Code.new(options: code_options)

    tasks = []
    tasks << TerraformWrapper::Tasks::Apply.new(binary: binary, code: code, options: config_options)
    tasks << TerraformWrapper::Tasks::Binary.new(binary: binary)
    tasks << TerraformWrapper::Tasks::Clean.new(code: code)
    tasks << TerraformWrapper::Tasks::Destroy.new(binary: binary, code: code, options: config_options)
    tasks << TerraformWrapper::Tasks::Fmt.new(binary: binary, code: code)
    tasks << TerraformWrapper::Tasks::Import.new(binary: binary, code: code, options: config_options)
    tasks << TerraformWrapper::Tasks::Init.new(binary: binary, code: code, options: config_options)
    tasks << TerraformWrapper::Tasks::Plan.new(binary: binary, code: code, options: config_options)
    tasks << TerraformWrapper::Tasks::PlanDestroy.new(binary: binary, code: code, options: config_options)
    tasks << TerraformWrapper::Tasks::Taint.new(binary: binary, code: code, options: config_options)
    tasks << TerraformWrapper::Tasks::Upgrade.new(binary: binary, code: code)
    tasks << TerraformWrapper::Tasks::Validate.new(binary: binary, code: code)
    tasks
  end

  #############################################################################
end

###############################################################################
