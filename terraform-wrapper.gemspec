###############################################################################

# frozen_string_literal: true

###############################################################################

require_relative 'lib/terraform-wrapper/version'

###############################################################################

Gem::Specification.new do |spec|
  spec.name        = 'terraform-wrapper'
  spec.version     = TerraformWrapper::VERSION
  spec.authors     = ['Richard Lees']
  spec.email       = ['git0@bitservices.io']

  spec.summary     = 'A ruby wrapper for managing Terraform binaries and remote state.'
  spec.description = 'A ruby wrapper for managing Terraform binaries and remote state. Each Terraform command (plan, apply, etc) is wrapped so that the correct binary is used and remote state referenced.'
  spec.homepage    = 'https://gitlab.com/bitservices/ruby/terraform-wrapper/'
  spec.license     = 'MIT'

  spec.required_ruby_version = Gem::Requirement.new('>= 2.6.0')

  spec.metadata['homepage_uri']    = spec.homepage
  spec.metadata['source_code_uri'] = spec.homepage

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      f.match(%r{\A(?:archlinux|test|spec|features)/}) or f.match(/\Aavatar(?:_group)?\.png\z/)
    end
  end

  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'rake', '~> 13.0'
  spec.add_dependency 'rubyzip', '~> 2.3'

  spec.add_development_dependency 'rspec', '~> 3.10'
end

###############################################################################
