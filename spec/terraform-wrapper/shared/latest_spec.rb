###############################################################################

shared_examples "latest version invalid response from hashicorp" do |name|
  describe "invalid response #{name}" do
    before(:each) do
      response = Net::HTTPResponse.new(http_version, code.to_s, message)
      allow_any_instance_of(Net::HTTP).to receive(:request) { response }
      allow(response).to receive(:body) { body }
      stub_const('Net::HTTPResponse::HAS_BODY', true)
    end

    it "must exit" do
      expect { TerraformWrapper::Shared::Latest.instance.version }.to raise_error(SystemExit)
    end

    it "must have error code 1" do
      begin
        TerraformWrapper::Shared::Latest.instance.version
      rescue SystemExit => e
        expect(e.status).to eq(1)
      end
    end
  end
end

###############################################################################

describe TerraformWrapper::Shared::Latest do
  before(:each) do
    @http_version = "1.1"
  end

  it_behaves_like "latest version invalid response from hashicorp", "404 not found" do
    let(:http_version) { @http_version }
    let(:code)         { 404 }
    let(:message)      { "Not Found" }
    let(:body)         { '{"error":"Not Found"}' }
  end

  it_behaves_like "latest version invalid response from hashicorp", "invalid JSON" do
    let(:http_version) { @http_version }
    let(:code)         { 200 }
    let(:message)      { "OK" }
    let(:body)         { 'Not JSON' }
  end

  it_behaves_like "latest version invalid response from hashicorp", "no version in JSON" do
    let(:http_version) { @http_version }
    let(:code)         { 200 }
    let(:message)      { "OK" }
    let(:body)         { '{"status":"ok"}' }
  end

  it_behaves_like "latest version invalid response from hashicorp", "blank version in JSON" do
    let(:http_version) { @http_version }
    let(:code)         { 200 }
    let(:message)      { "OK" }
    let(:body)         { '{"current_version":""}' }
  end

  describe "latest version valid response from hashicorp" do
    it "must return the version provided by hashicorp" do
      response = Net::HTTPResponse.new(@http_version, "200", "OK")
      allow_any_instance_of(Net::HTTP).to receive(:request) { response }
      allow(response).to receive(:body) { '{"current_version":"1.0.0"}' }
      stub_const('Net::HTTPResponse::HAS_BODY', true)
      expect(TerraformWrapper::Shared::Latest.instance.version).to eql("1.0.0")
    end
  end
end

###############################################################################
