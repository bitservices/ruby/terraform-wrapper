###############################################################################

shared_examples "variables adding invalid files" do |name|
  describe "passing #{name} as variables file" do
    it "must exit" do
      expect { variables.add_files(base: Dir.pwd, files: files) }.to raise_error(SystemExit)
    end

    it "must have error code 1" do
      begin
        variables.add_files(base: Dir.pwd, files: files)
      rescue SystemExit => e
        expect(e.status).to eq(1)
      end
    end
  end
end

###############################################################################

shared_examples "variables adding invalid hash" do |name|
  describe "passing #{name} as variables hash" do
    it "must exit" do
      expect { variables.add_variables(variables: hash) }.to raise_error(SystemExit)
    end

    it "must have error code 1" do
      begin
        variables.add_variables(variables: hash)
      rescue SystemExit => e
        expect(e.status).to eq(1)
      end
    end
  end
end

###############################################################################

shared_examples "variables adding valid hash" do |name|
  describe "passing #{name} variables hash" do
    it "must add and process valid variables" do
      begin
        variables.add_variables(variables: hash)
      rescue SystemExit => e
        raise "Test failed!"
      end

      expect(variables.values[key]).to eql(value)
    end
  end
end

###############################################################################

shared_examples "variables identifier is set and has correct value" do |name|
  it "#{name}" do
    expect(variables.identifiers[name.to_sym]).to eql(value)
  end
end

###############################################################################

shared_examples "variables with invalid identifiers" do |name|
  describe "passing #{name} as identifiers" do
    it "must exit" do
      expect { TerraformWrapper::Shared::Variables.new(component: @component, config: @config, service: @service, identifiers: option) }.to raise_error(SystemExit)
    end

    it "must have error code 1" do
      begin
        TerraformWrapper::Shared::Variables.new(component: @component, config: @config, service: @service, identifiers: option)
      rescue SystemExit => e
        expect(e.status).to eq(1)
      end
    end
  end
end

###############################################################################

describe TerraformWrapper::Shared::Variables do
  before(:all) do
    @component = "rspec"
    @config    = "rspec1"
    @service   = "test"

    @files_base             = File.join(Dir.pwd, "tfvars")
    @files_ext              = ".tfvars"
    @files_invalid_baditems = [ Hash.new, nil, "badfile" ]
    @files_invalid_blank    = [ " " ]
    @files_invalid_hash     = Hash.new
    @files_invalid_notexist = [ "badfile"  ]
    @files_invalid_string   = "badfile"
    @files_mocked           = [ "goodfile" ]

    @identifiers_case_value       = "UPPER"
    @identifiers_invalid_array    = Array.new
    @identifiers_invalid_reserved = { "service" => "failed" }
    @identifiers_invalid_string   = "hello"
    @identifiers_valid            = { "network" => "testnet", "account" => "testing", "CASE" => @identifiers_case_value }

    @variables_invalid_array      = Array.new
    @variables_invalid_blank      = { "blank" => "" }
    @variables_invalid_key        = { Hash.new => "value" }
    @variables_invalid_string     = "invalid"
    @variables_invalid_identifier = { "bucket" => "%{stream}" }
    @variables_invalid_value_type = { "array" => Array.new }
    @variables_valid_stripping    = { "space" => "   test " }
    @variables_valid_templating   = { "group" => "%{case}-group", "before" => "before" }
  end

  it_behaves_like "variables with invalid identifiers", "nil" do
    let(:option) { nil }
  end

  it_behaves_like "variables with invalid identifiers", "array" do
    let(:option) { @identifiers_invalid_array }
  end

  it_behaves_like "variables with invalid identifiers", "string" do
    let(:option) { @identifiers_invalid_string }
  end

  it_behaves_like "variables with invalid identifiers", "reserved" do
    let(:option) { @identifiers_invalid_reserved }
  end

  describe "variables with sorted identifiers" do
    let(:variables) { TerraformWrapper::Shared::Variables.new(component: @component, config: @config, service: @service, identifiers: @identifiers_valid, sort: true) }
    it "must have the identifiers automatically sorted" do
      expect(variables.identifiers.find_index { |k,_| k == :network }).to be > variables.identifiers.find_index { |k,_| k == :account }
    end
  end

  describe "variables with unsorted identifers" do
    let(:variables) { TerraformWrapper::Shared::Variables.new(component: @component, config: @config, service: @service, identifiers: @identifiers_valid, sort: false) }
    it "must have the identifiers returned in the same order that they were provided" do
      expect(variables.identifiers.find_index { |k,_| k == :account }).to be > variables.identifiers.find_index { |k,_| k == :network }
    end
  end

  describe "variables tests that are unrelated to sorting of identifiers" do
    let(:variables) { TerraformWrapper::Shared::Variables.new(component: @component, config: @config, service: @service, identifiers: @identifiers_valid) }

    describe "core identifiers are present" do
      it_behaves_like "variables identifier is set and has correct value", "component" do
        let(:value) { @component }
      end

      it_behaves_like "variables identifier is set and has correct value", "config" do
        let(:value) { @config }
      end

      it_behaves_like "variables identifier is set and has correct value", "service" do
        let(:value) { @service }
      end
    end

    describe "identifiers are automatically convered to lowercase symbols" do
      it_behaves_like "variables identifier is set and has correct value", "case" do
        let(:value) { @identifiers_case_value }
      end
    end

    describe "adding variable files" do
      it_behaves_like "variables adding invalid files", "nil" do
        let(:files) { nil }
      end

      it_behaves_like "variables adding invalid files", "hash" do
        let(:files) { @files_invalid_hash }
      end

      it_behaves_like "variables adding invalid files", "string" do
        let(:files) { @files_invalid_string }
      end

      it_behaves_like "variables adding invalid files", "non-string array" do
        let(:files) { @files_invalid_baditems }
      end

      it_behaves_like "variables adding invalid files", "blank string array" do
        let(:files) { @files_invalid_blank }
      end

      it_behaves_like "variables adding invalid files", "not existing file" do
        let(:files) { @files_invalid_notexist }
      end

      describe "adding variables files that do exist" do
        before(:each) do
          @paths_expected = Array.new
          @files_mocked.each do |file|
            path = File.join(@files_base, "#{file}#{@files_ext}")
            allow(File).to receive(:file?).and_call_original
            allow(File).to receive(:file?).with(path).and_return(true)
            @paths_expected << path
          end
        end

        describe "can add a variable file if it exists" do
          it "must add the variables file" do
            begin
              variables.add_files(base: Dir.pwd, files: @files_mocked)
            rescue SystemExit => e
              raise "Test failed!"
            end

            @paths_expected.each do |path|
              expect(variables.files).to include(path)
            end
          end
        end

        describe "skip adding duplicate files" do
          it "must only add the variables file once" do
            begin
              variables.add_files(base: Dir.pwd, files: @files_mocked)
              variables.add_files(base: Dir.pwd, files: @files_mocked.concat(@files_mocked))
            rescue SystemExit => e
              raise "Test failed!"
            end

            @paths_expected.each do |path|
              expect(variables.files.tally[path]).to eql(1)
            end
          end
        end
      end
    end

    describe "adding variables" do
      it_behaves_like "variables adding invalid hash", "nil" do
        let(:hash) { nil }
      end

      it_behaves_like "variables adding invalid hash", "array" do
        let(:hash) { @variables_invalid_array }
      end

      it_behaves_like "variables adding invalid hash", "string" do
        let(:hash) { @variables_invalid_string }
      end

      it_behaves_like "variables adding invalid hash", "array variable" do
        let(:hash) { @variables_invalid_value_type }
      end

      it_behaves_like "variables adding invalid hash", "blank variable" do
        let(:hash) { @variables_invalid_blank }
      end

      it_behaves_like "variables adding invalid hash", "hash key" do
        let(:hash) { @variables_invalid_key }
      end

      it_behaves_like "variables adding invalid hash", "templated identifer that does not exist" do
        let(:hash) { @variables_invalid_identifier }
      end

      it_behaves_like "variables adding valid hash", "stripped" do
        let(:hash) { @variables_valid_stripping }
        let(:key) { :space }
        let(:value) { "test" }
      end

      it_behaves_like "variables adding valid hash", "templated" do
        let(:hash) { @variables_valid_templating }
        let(:key) { :group }
        let(:value) { "#{@identifiers_case_value}-group" }
      end

      describe "fail adding duplicate variables" do
        it "must succeed first time, fail second time" do
          begin
            variables.add_variables(variables: @variables_valid_stripping)
          rescue SystemExit => e
            raise "Test failed!"
          end

          expect { variables.add_variables(variables: @variables_valid_stripping) }.to raise_error(SystemExit)
        end

        it "must succeed first time, have error code 1 second time" do
          begin
            variables.add_variables(variables: @variables_valid_stripping)
          rescue SystemExit => e
            raise "Test failed!"
          end

          begin
            variables.add_variables(variables: @variables_valid_stripping)
          rescue SystemExit => e
            expect(e.status).to eq(1)
          end
        end
      end

      describe "variables added with sorting" do
        it "must have the variables returned in the same order that they were provided" do
          begin
            variables.add_variables(variables: @variables_valid_templating, sort: true)
          rescue SystemExit => e
            raise "Test failed!"
          end

          expect(variables.values.find_index { |k,_| k == :group }).to be > variables.values.find_index { |k,_| k == :before }
        end
      end

      describe "variables added unsorted" do
        it "must have the variables returned in the same order that they were provided" do
          begin
            variables.add_variables(variables: @variables_valid_templating)
          rescue SystemExit => e
            raise "Test failed!"
          end

          expect(variables.values.find_index { |k,_| k == :before }).to be > variables.values.find_index { |k,_| k == :group }
        end
      end
    end
  end
end

###############################################################################
