###############################################################################

shared_examples "provider with invalid platforms option" do |name|
  describe "passing #{name} as platforms option" do
    let (:provider_options) { { "platforms" => option } }

    it "must exit" do
      expect { TerraformWrapper::Shared::Provider.new(options: provider_options) }.to raise_error(SystemExit)
    end

    it "must have error code 1" do
      begin
        TerraformWrapper::Shared::Provider.new(options: provider_options)
      rescue SystemExit => e
        expect(e.status).to eq(1)
      end
    end
  end
end

###############################################################################

describe TerraformWrapper::Shared::Provider do
  before(:all) do
    @testarray  = [ "linux_amd64", "darwin_amd64" ]
    @testhash   = Hash.new
    @teststring = "test"
  end

  describe "provider with valid platforms option" do
    let (:provider_options) { { "platforms" => @testarray } }

    it "must return the same providers array that was given at creation" do
      expect(TerraformWrapper::Shared::Provider.new(options: provider_options).platforms).to eql(@testarray)
    end
  end

  it_behaves_like "provider with invalid platforms option", "nil" do
    let(:option) { nil }
  end

  it_behaves_like "provider with invalid platforms option", "hash" do
    let(:option) { @testhash }
  end

  it_behaves_like "provider with invalid platforms option", "string" do
    let(:option) { @teststring }
  end
end

###############################################################################
